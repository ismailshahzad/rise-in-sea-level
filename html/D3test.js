j=0;
k=0;
data_1 = []
data_2 = []
//Helping material used 
//Read D3 Tips and Tricks v4.x. (n.d.). Retrieved from https://leanpub.com/d3-t-and-t-v4/read.
// set the dimensions and margins of the graph
var graphMargin = {top:30, bottom:30, right:30, left:100}
{
    width = 700 - graphMargin.left - graphMargin.right;
    height = 450 - graphMargin.top - graphMargin.bottom;
}
 // parse the date/time and get Date in date time format and price as a number
 var parseDate = d3.timeParse("%Y-%m-%d");
 //reading csv from github
d3.csv('https://gitlab.com/ismailshahzad/rise-in-sea-level/raw/master/sealevel.csv')
.row(function(d){return{time:parseDate(d.Time),CW:Number(d.CW_change_mm),UHSLCFD:Number(d.UHSLCFD_change_mm)};})
.get(function(error,data)
{
    

    for (var i=0;i<data.length;i++)
    {
        if (i >= 118 & i<638)
        {
        data_1[j] = data[i]
        j=j+1;
        }
    }
    for (var l=0;l<data.length;l++)
    {
        if (l >477 )
        {
        data_2[k] = data[l]
        k=k+1;
        }
    }
 //finding min,max of date and price
 var maxDate = d3.max(data,function(d){ return d.time;});
 var minDate = d3.min(data,function(d){ return d.time;});
 var maxVal = d3.max(data,function(d){ return d.UHSLCFD;});
 var minVal = d3.min(data,function(d){ return d.CW;});
//var minDate = 1700
 console.log(maxDate)
console.log(minDate)
console.log(maxVal)
console.log(minVal)
console.log(data_1)
 //setting ranges by giving minDate and maxDate as a domain and using width
    // and height as range for scaled values
    var y = d3.scaleLinear()
    .domain([minVal,maxVal])
    .range([height,0]);

    var x = d3.scaleTime()
    .domain([minDate,maxDate])
    .range([0,width]);



    // define the line
      var linegenerator = d3.line()
       .x(function(d) { return x(d.time); })  
       .y(function(d) { return y(d.CW); });




// define the 2nd line
var linegenerator2 = d3.line()
    .x(function(d) { return x(d.time); })
    .y(function(d) { return y(d.UHSLCFD); });


       // append the svg obgect to the body of the page
// appends a 'group' element to 'svg'
// moves the 'group' element to the top left margin
var svg = d3.select("body").append("svg")   
.attr("width", width + graphMargin.left + graphMargin.right  )
.attr("height", height + graphMargin.top + graphMargin.bottom +20 )
.append("g")
.attr("transform",
  "translate(" + graphMargin.left + "," + graphMargin.top + ")");

// Add the linegenerator path.
svg.append("path")
  .data([data_1])
  .attr("class", "lines")
  .attr("data-legend",function(d) { return d.Cw})
  .attr("d", linegenerator);
  
    // Add the valueline2 path.
    svg.append("path")
    .data([data_2])
    .style("stroke", "red")
    .attr("class", "lines")
    .attr("d", linegenerator2);

// Add the x Axis
svg.append("g")
//   .attr("transform", "translate(0,280)")
  .attr("transform", "translate(0," + height + ")")
  .call(d3.axisBottom(x));

// text label for the x axis
svg.append("text")              
  .attr("transform",
        "translate(" + (width/2) + " ," + (height + graphMargin.top + 5) + ")")
        .style("text-anchor", "middle")
  .text("Year");
// Add the y Axis
svg.append("g")
  .call(d3.axisLeft(y));
// text label for the y axis
svg.append("text")
  .attr("transform", "rotate(-90)")
  .attr("y", 0 - 50)
  .attr("x",0 - (height / 2))
  .attr("dy", "1em")
  .style("text-anchor", "middle")
  .text("Change in water level in mm compared to average of 1993-2009");  
  
  legend = svg.append("g")
    .attr("class","legend")
    .attr("transform","translate(50,30)")
    .style("font-size","12px")
    .call(d3.legend)



});

