import pandas as pd
import datetime


data = pd.read_csv('ftp://ftp.csiro.au/legresy/gmsl_files/CSIRO_Alt_seas_inc.csv')

data['Time'] = data['Time'].astype(int)

data = data.groupby(data['Time'])['GMSL (monthly)'].mean()

data = data.reset_index()

data.columns = ['Year' , 'GMSL']

data['GMSL'] = round(data['GMSL'],2)


data.to_csv('CISROsealevel.csv',index=False)