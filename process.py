import bs4
from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup

#URL for getting the data from chart
my_url = 'https://www.climate.gov/gcd/multigraph-mugl/html5-graph-sealevel'

# Opening connection and grabbing the page
uClient = uReq(my_url)
page_html = uClient.read()
uClient.close()

#HTML parsing
page_soup = soup(page_html, "lxml")

#Get the required data from the page
print_data = page_soup.mugl.data.text.strip()
data_split = print_data.split('\n')
#Write the data to csv
filename = 'sealevel.csv'
f = open(filename,"w")

#Set the headers and write the data to csv
headers = 'Time,CW_change_mm,UHSLCFD_change_mm'
f.write(headers)

#Write the data values in csv line by line
for value in page_soup.mugl.data.values.strings:
    f.write(value)
f.close()




