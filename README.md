# Change in sea level over time

* This is an overview of the change in sea levels from 1800s to the present date compared to the (1993-2008)Average.
* Global mean sea level from 1993-2019 timeseries.

## Data
### Data for 1883-present date compared to average of 1993-2008:
Data is obtained from https://www.climate.gov/news-features/understanding-climate/climate-change-global-sea-level  
The dataset is a mixture of data obtained from two sources:
1. [Research work by Church and White](https://link.springer.com/article/10.1007%2Fs10712-011-9119-1): They estimated the data of changes in sea level from 1880s to 2009.
2. [University of Hawaii Sea Level Center](http://uhslc.soest.hawaii.edu/data/?fd): Data after 2009, is obtained from UHSLC. It is based on a weighted average of 373 global tide gauge records collected by the U.S. National Ocean Service, UHSLC and partner agencies worldwide.  
The values in the dataset are change in sea level in millimeters compared to the 1993-2008 average(ie. Almost 20mm).  

This data can be viewed in sealevel.csv file

### Data for Global mean sea level timeseries from 1993-2019: 
Data is obtained from the ftp server of The Commonwealth Scientific and Industrial Research Organisation (CSIRO): [CSIRO Data](ftp://ftp.csiro.au/legresy/gmsl_files)  
This data can be viewed in CSIROsealevel.csv
### Other good sources of data:
* [Sea level data by NASA](https://climate.nasa.gov/vital-signs/sea-level/) : This data source provides data of change in sea level in mm from 1993 to present date.


## Preparation
* The process.py script fetches data from the chart on [climate.gov website](https://www.climate.gov/news-features/understanding-climate/climate-change-global-sea-level)
* The data is in xml page the script extracts the data and writes it to sealevel.csv file.
* The process_gm.py script fetches data from CSIRO takes the mean for each year using monthly values and writes the global mean for each year in CSIROsealevel.csv
* Libraries used urlib,bs4,pandas.

## Files
* sea-level.csv             - complete data of change in sea levels compared to the average of 1993-2008
* CSIROsealevel.csv         - CSIRO data for global mean sea level per year
* README.md                 - readme file
* process.py                - Python script for processing sea-level.csv data
* process_gm.py             - Python script for processing CISRO data
* datapackage.json          - json datapackage file 
* html folder
    * D3test.js                         -The js file for index.html(includes the html scripts)
    * index.css                         -css file for index.html
    * index.html                        -Main html file to show visualization for sealevel.csv filw
    * gmsl.js                           -The js file for GMSL visualization.html
    * GMSL visualization.html           -The html file for Global mean sea level visualization
    * GMSL visualization.css            -The js file for html(includes the html scripts)

## How to run
* You need to run `process.py` and `process_gm.py` file in python.
* For html you need to run `index.html` file. 

## License
[Public Domain Dedication and License v1.0](http://www.opendatacommons.org/licenses/pddl/1.0/)

## Visualization
### Visualization of changes in sea level from 1883 to present date compared to the average of 1993-2008
* The blue line shows Church and White estimate
* The red line shows UHSLC estimate
![](/img/D3.PNG)

### Global mean sea level(GMSL) Time series
![](/img/D3gmsl.PNG)